import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Facilities} from '../models/facilities.model';
import {Facility} from '../models/facility.model';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';

const ENDPOINT_URL = environment.endpointURL;

@Injectable({
    providedIn: 'root'
})
export class FacilityService {
    private _results: Facilities;
    public searchTerm: string;
    private _facility: Facility;
    private _isSearching: boolean;

    geoLatitude: number;
    geoLongitude: number;
    geoAccuracy:number;
    geoAddress: string;
    watchLocationUpdates:any;
    loading:any;
    isWatching:boolean;
    // Geocoder configuration
    geoEncoderOptions: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
    };

    searching() {
        this._isSearching = true;
    }
    notSearching() {
        this._isSearching = false;
    }

    isSearching() {
        return this._isSearching;
    }

    getResults(): Facilities {
        return this._results;
    }

    setResults(value: Facilities) {
        this._results = value;
    }

    getFacility(): Facility {
        return this._facility;
    }

    setFacility(value: Facility) {
        this._facility = value;
    }

    constructor(
        private http: HttpClient,
        private geolocation: Geolocation,
        private nativeGeocoder: NativeGeocoder
    ) {}

    search(event) {
        this.searchTerm = event.srcElement.value;
        if (!this.searchTerm) {
            return;
        }

        this.setResults(new Facilities());
        this.searching();
        return this.http.get(`${ENDPOINT_URL}/search?per_page=20&location=${encodeURI(this.searchTerm)}`)
            .pipe(
                map((facilities: Facilities) => new Facilities().constructorFromJSON(facilities.meta, facilities.data)),
            ).subscribe(
                results => this._results = results,
                error  => {
                    console.log('TODO error handler', error);
                    this.searching();
                },
                () => {
                    this.notSearching();
                }
            );
    }

    searchByLatLong(latitude, longitude) {
        this.setResults(new Facilities());
        this.searching();
        return this.http.get(`${ENDPOINT_URL}/search?per_page=20&lat=${encodeURI(latitude)}&lon=${encodeURI(longitude)}`)
            .pipe(
                map((facilities: Facilities) => new Facilities().constructorFromJSON(facilities.meta, facilities.data)),
            ).subscribe(
                results => this._results = results,
                error  => {
                    console.log('TODO error handler', error);
                    this.searching();
                },
                () => {
                    this.notSearching();
                }
            );
    }

    retrieveFacility(facilityId: string) {
        return this.http.get(`${ENDPOINT_URL}/${facilityId}`);
    }

    // Get current coordinates of device
    getGeolocation() {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.geoLatitude = resp.coords.latitude;
            this.geoLongitude = resp.coords.longitude;
            this.geoAccuracy = resp.coords.accuracy;
            this.searchByLatLong(this.geoLatitude, this.geoLongitude);
        }).catch((error) => {
            alert('Error getting location' + JSON.stringify(error));
        });
    }
}
