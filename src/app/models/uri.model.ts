import {Deserializable} from './deserializable.model';

export class Uris implements Deserializable {
    first: string;
    last: string;
    next: string;
    prev?: any;
    self: string;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}


