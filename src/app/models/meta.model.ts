import {Uris} from './uri.model';

export class Meta {
    total: number;
    per_page: number;
    current_page: number;
    filter: any[];
    order: any[];
    uris: Uris;

    constructorFromJSON(
        total: any,
        per_page: any,
        current_page: any,
        filter: any,
        order: any,
        uris: any
    ) {
        this.total = total;
        this.per_page = per_page;
        this.current_page = current_page;
        this.filter = filter;
        this.order = order;
        this.uris = uris;

        return this;
    }

}
