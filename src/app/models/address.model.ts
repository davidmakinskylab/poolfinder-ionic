export class Address {
    line_one: string;
    line_two: string;
    city: string;
    area?: any;
    country: string;
    post_code: string;
    created_at: Date;
    updated_at: Date;
}
