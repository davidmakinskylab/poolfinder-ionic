import {Meta} from './meta.model';
import {FacilityWithDistanceModel} from './facilityWithDistance.model';

export class Facilities {
    meta: Meta;
    data: FacilityWithDistanceModel[];

    constructorFromJSON(meta, data) {
        this.meta = new Meta().constructorFromJSON(
            meta.total,
            meta.per_page,
            meta.current_page,
            meta.filter,
            meta.order,
            meta.uris,
        );
        this.data = data.map(
            (facility: FacilityWithDistanceModel) => new FacilityWithDistanceModel()
                .constructorFromJSON(facility.distance, facility.facility)
        );

        return this;
    }
}
