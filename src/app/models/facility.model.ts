import {Address} from './address.model';
import {Pool} from './pool.model';
import {Coordinates} from './coordinates.model';
import {Club} from './club.model';
import {Facilities} from './facilities.model';
import {FacilityWithDistanceModel} from './facilityWithDistance.model';

export class Facility {
    readonly POOL_STATUS_OPEN = 1;
    readonly POOL_ACCESS_TYPE_PRIVATE = 3;

    id: number;
    name: string;
    email_address: string;
    website: string;
    telephone: string;
    carpark: boolean;
    swimfit: boolean;
    bigsplash: boolean;
    child_lessons: boolean;
    child_learn_to_swim: boolean;
    adult_lessons: boolean;
    adult_learn_to_swim: boolean;
    asa_badges: boolean;
    women_only: boolean;
    aquatic_fitness: boolean;
    swim_britian_pool: boolean;
    swim_britian_events: boolean;
    swim_britian_training: boolean;
    swim_school: boolean;
    disney_sessions: boolean;
    address: Address;
    coordinates: Coordinates;
    pools: Pool[] = [];
    clubs: Club[] = [];
    swim_schools: any[];
    created_at: Date;
    updated_at: Date;
    has25mPool: boolean;
    has50mPool: boolean;
    hasDisabledAccess: boolean;
    hasDivingBoard: boolean;
    hasOpenPool: boolean;
    clubTypes: Array<any> = [];

    constructorFromJSON (facility) {
        if (facility.id) {
            this.id = facility.id;
        }
        if (facility.name) {
            this.name = facility.name;
        }
        if (facility.email_address) {
            this.email_address = facility.email_address;
        }
        if (facility.website) {
            this.website = facility.website;
        }
        if (facility.telephone) {
            this.telephone = facility.telephone;
        }
        if (facility.carpark) {
            this.carpark = facility.carpark;
        }
        if (facility.swimfit) {
            this.swimfit = facility.swimfit;
        }
        if (facility.bigsplash) {
            this.bigsplash = facility.bigsplash;
        }
        if (facility.child_lessons) {
            this.child_lessons = facility.child_lessons;
        }
        if (facility.child_learn_to_swim) {
            this.child_learn_to_swim = facility.child_learn_to_swim;
        }
        if (facility.adult_lessons) {
            this.adult_lessons = facility.adult_lessons;
        }
        if (facility.adult_learn_to_swim) {
            this.adult_learn_to_swim = facility.adult_learn_to_swim;
        }
        if (facility.asa_badges) {
            this.asa_badges = facility.asa_badges;
        }
        if (facility.women_only) {
            this.women_only = facility.women_only;
        }
        if (facility.aquatic_fitness) {
            this.aquatic_fitness = facility.aquatic_fitness;
        }
        if (facility.swim_britian_pool) {
            this.swim_britian_pool = facility.swim_britian_pool;
        }
        if (facility.swim_britian_events) {
            this.swim_britian_events = facility.swim_britian_events;
        }
        if (facility.swim_britian_training) {
            this.swim_britian_training = facility.swim_britian_training;
        }
        if (facility.swim_school) {
            this.swim_school = facility.swim_school;
        }
        if (facility.disney_sessions) {
            this.disney_sessions = facility.disney_sessions;
        }
        if (facility.address) {
            this.address = facility.address; // TODO new Address(line1, line2, etc)
        }
        if (facility.coordinates) {
            this.coordinates = facility.coordinates;
        }
        if (facility.pools) {
            this.pools = facility.pools;
        }
        if (facility.clubs) {
            for (const club of facility.clubs) {
                const tmpClub = new Club().constructorFromJSON(club);
                this.clubs.push(tmpClub);

                if (tmpClub.has_beginners) {
                    this.clubTypes.push('beginners');
                }
                if (tmpClub.has_disabled) {
                    this.clubTypes.push('disabled');
                }
                if (tmpClub.has_juniors) {
                    this.clubTypes.push('juniors');
                }
                if (tmpClub.has_seniors) {
                    this.clubTypes.push('seniors');
                }
                if (tmpClub.has_students) {
                    this.clubTypes.push('students');
                }
                if (tmpClub.has_veterans) {
                    this.clubTypes.push('veterans');
                }
            }
        }
        if (facility.swim_schools) {
            this.swim_schools = facility.swim_schools;
        }
        if (facility.created_at) {
            this.created_at = facility.created_at;
        }
        if (facility.updated_at) {
            this.updated_at = facility.updated_at;
        }

        this.setFacilityAttributesFromPools();
        return this;
    }

    setFacilityAttributesFromPools()  {
        for (const pool of this.pools) {
            if (pool.length >= 50) {
                this.has50mPool = true;
                this.has25mPool = true;
            } else if (pool.length >= 25) {
                this.has25mPool = true;
            }

            if (pool.disabled_access) {
                this.hasDisabledAccess = true;
            }

            if (pool.diving_board) {
                this.hasDivingBoard = true;
            }

            if (
                pool.status.id === this.POOL_STATUS_OPEN &&
                pool.access_type.id !== this.POOL_ACCESS_TYPE_PRIVATE
            ) {
                this.hasOpenPool = true;
            }

            // Only loop around the pools until all have been set to true
            if (this.has25mPool &&
                this.has50mPool &&
                this.hasDisabledAccess &&
                this.hasDivingBoard &&
                this.hasOpenPool
            ) {
                continue;
            }
        }
    }

    getFacilityCLubTypes() {
        for (const club of this.clubs) {
            // if (club.d)
        }
    }
}

