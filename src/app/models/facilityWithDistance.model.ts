import {Facility} from './facility.model';

export class FacilityWithDistanceModel {
    distance: number;
    facility: Facility;

    constructorFromJSON(distance, facility) {
        this.distance = distance;
        this.facility = new Facility().constructorFromJSON(facility);

        return this;
    }

    distanceInMiles() {
        return String(this.distance * 0.000621371);
    }
}
