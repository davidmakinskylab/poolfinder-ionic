import {Type} from './type.model';
import {AccessType} from './accessType.model';
import {Status} from './status.model';

export class Club {
    id: number;
    code: string;
    name: string;
    website: string;
    number_of_members: number;
    has_beginners: boolean;
    has_juniors: boolean;
    has_seniors: boolean;
    has_veterans: boolean;
    has_students: boolean;
    has_disabled: boolean;
    disciplines: any;
    created_at: Date;
    updated_at: Date;

    constructorFromJSON (club) {
        if (club.id) {
            this.id = club.id;
        }
        if (club.code) {
            this.code = club.code;
        }
        if (club.name) {
            this.name = club.name;
        }
        if (club.website) {
            this.website = club.website;
        }
        if (club.number_of_members) {
            this.number_of_members = club.number_of_members;
        }
        if (club.has_beginners) {
            this.has_beginners = club.has_beginners;
        }
        if (club.has_juniors) {
            this.has_juniors = club.has_juniors;
        }
        if (club.has_seniors) {
            this.has_seniors = club.has_seniors;
        }
        if (club.has_veterans) {
            this.has_veterans = club.has_veterans;
        }
        if (club.has_students) {
            this.has_students = club.has_students;
        }
        if (club.has_disabled) {
            this.has_disabled = club.has_disabled;
        }
        if (club.disciplines) {
            this.disciplines = club.disciplines;
        }
        if (club.created_at) {
            this.created_at = club.created_at;
        }
        if (club.updated_at) {
            this.updated_at = club.updated_at;
        }

        return this;
    }
}
