import {Type} from './type.model';
import {AccessType} from './accessType.model';
import {Status} from './status.model';

export class Pool {
    id: number;
    type: Type;
    access_type: AccessType;
    status: Status;
    opening_hours: string;
    year_built?: number;
    width: number;
    length: number;
    area: number;
    depth_shallow: number;
    depth_deep: number;
    disabled_access: boolean;
    diving_board: boolean;
    movable_floor: boolean;
    created_at: Date;
    updated_at: Date;

    is50MetrePool() {
        return (this.length > 50);
    }
}
