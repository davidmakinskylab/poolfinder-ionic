import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'facilities', pathMatch: 'full'},
    {path: 'facilities', loadChildren: './pages/facilities/facilities.module#FacilitiesPageModule'},
    {path: 'facility/:id', loadChildren: './pages/facility/facility.module#FacilityPageModule'},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
