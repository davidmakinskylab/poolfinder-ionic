import {CUSTOM_ELEMENTS_SCHEMA, NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';

import {GlobalHeaderComponent} from './global-header/global-header.component';
import {GlobalHeaderConfig} from './global-header/global-header.config';
import {FacilityStatsComponent} from './facility-stats/facility-stats.component';
import {FacilityClubsComponent} from './facility-clubs/facility-clubs.component';

@NgModule({
    imports: [
        CommonModule,
        IonicModule
    ],
    declarations: [
        GlobalHeaderComponent,
        FacilityStatsComponent,
        FacilityClubsComponent
    ],
    exports: [
        GlobalHeaderComponent,
        FacilityStatsComponent,
        FacilityClubsComponent
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class SharedModule {

    static forRoot(config: GlobalHeaderConfig): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [{
                provide: GlobalHeaderConfig,
                useValue: config
            }]
        };
    }

    static forChild(): ModuleWithProviders {
        return {
            ngModule: SharedModule
        };
    }
}
