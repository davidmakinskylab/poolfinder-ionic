import {Component, Input, OnInit} from '@angular/core';
import {Facility} from '../../models/facility.model';

@Component({
    selector: 'app-facility-clubs',
    templateUrl: './facility-clubs.component.html',
    styleUrls: ['./facility-clubs.component.scss'],
})
export class FacilityClubsComponent implements OnInit {
    @Input() page: string;
    @Input() facility: Facility;

    constructor() { }

    ngOnInit() {}

}
