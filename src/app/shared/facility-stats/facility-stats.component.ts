import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import {Facility} from '../../models/facility.model';

@Component({
  selector: 'app-facility-stats',
  templateUrl: './facility-stats.component.html',
  styleUrls: ['./facility-stats.component.scss'],
})
export class FacilityStatsComponent implements OnInit {
  @Input() page: string;
  @Input() facility: Facility;
  constructor() { }

  ngOnInit() {}

}
