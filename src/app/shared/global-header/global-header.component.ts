import {Component, OnInit} from '@angular/core';

import {GlobalHeaderConfig} from './global-header.config';
import {FacilityService} from '../../services/facility.service';

@Component({
    selector: 'app-global-header',
    templateUrl: './global-header.component.html',
    styleUrls: ['./global-header.component.scss'],
})
export class GlobalHeaderComponent implements OnInit {
    includeSearch: boolean;
    includeBack: boolean;

    constructor(config: GlobalHeaderConfig, public facilityService: FacilityService) {
        this.includeSearch = config.includeSearch;
        this.includeBack = config.includeBack;
        this.facilityService = facilityService;
    }

    ngOnInit() {}
}
