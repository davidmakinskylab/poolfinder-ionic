import {FacilityService} from '../../services/facility.service';
import {Component, ElementRef, NgModule, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {SharedModule} from '../../shared/shared.module';
import {GlobalHeaderConfig} from '../../shared/global-header/global-header.config';
import {environment} from '../../../environments/environment';
import {Facility} from '../../models/facility.model';

declare var google: any;

@Component({
    selector: 'app-facility',
    templateUrl: './facility.page.html',
    styleUrls: ['./facility.page.scss'],
})
@NgModule({
    imports: [
        SharedModule
    ]
})
export class FacilityPage implements OnInit {
    @ViewChild('map') mapElement: ElementRef;
    facility: Facility;
    map: any;
    mapOptions: any;
    location = {lat: null, lng: null};
    markerOptions: any = {position: null, map: null, title: null};
    marker: any;

    static loadGoogleMaps() {
        if (typeof google === 'undefined' || typeof google.maps === 'undefined') {
            const script = document.createElement('script');
            script.id = 'googleMap';
            script.src = 'https://maps.googleapis.com/maps/api/js?key=' + environment.gmapsAPIKey;
            document.head.appendChild(script);
        }
    }

    constructor(
        private geolocation: Geolocation,
        private activatedRoute: ActivatedRoute,
        private facilityService: FacilityService,
        private globalHeaderConfig: GlobalHeaderConfig
    ) {
        this.globalHeaderConfig.includeBack = true;
        this.globalHeaderConfig.includeSearch = false;
        this.facilityService = facilityService;

        FacilityPage.loadGoogleMaps();
    }

    ngOnInit() {
        // Get the ID that was passed with the URL
        const id = this.activatedRoute.snapshot.paramMap.get('id');
        let facility: any;

        if (this.facilityService.getFacility() && this.facilityService.getFacility().id.toString() === id) {
            this.facility = this.facilityService.getFacility();
            this.addFacilityMap();
        } else {
            // Get the facility from the API
            this.facilityService.retrieveFacility(id).subscribe(
                result => {
                    facility = result;
                },
                error => {
                    console.log('TODO error handler', error);
                },
                () => {
                    this.facility = new Facility()
                        .constructorFromJSON(facility);
                    this.addFacilityMap();
                }
            );
        }
    }

    addFacilityMap() {
        setTimeout(() => {
            const latLng = new google.maps.LatLng(this.facility.coordinates.lat, this.facility.coordinates.long);
            this.map = new google.maps.Map(
                this.mapElement.nativeElement,
                {
                    center: latLng,
                    zoom: 17,
                    mapTypeControl: false
                }
            );
        }, 3000);
    }

    openWebsite() {
        window.open(this.facility.website, '_blank');
    }

    openDirections() {
        window.open(
            'https://maps.google.co.uk/maps?saddr=Current+Location&daddr=@' +
                this.facility.coordinates.lat + ',' + this.facility.coordinates.long,
            '_blank'
        );
    }
}
