import {FacilityService} from '../../services/facility.service';
import {Component, NgModule, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {GlobalHeaderConfig} from '../../shared/global-header/global-header.config';
import {Facility} from '../../models/facility.model';

@Component({
    selector: 'app-facilities',
    templateUrl: './facilities.page.html',
    styleUrls: ['./facilities.page.scss'],
})
@NgModule({
    imports: [
        SharedModule
    ]
})
export class FacilitiesPage implements OnInit {

    constructor(
        private router: Router,
        public facilityService: FacilityService,
        public globalHeaderConfig: GlobalHeaderConfig
    ) {
        this.globalHeaderConfig.includeBack = false;
        this.globalHeaderConfig.includeSearch = true;
        this.facilityService = facilityService;
    }

    ngOnInit() {}

    getFacilityDetails(facility: Facility) {
        this.facilityService.setFacility(facility);
        this.router.navigate(['/facility', facility.id]);
    }
}
