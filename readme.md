ionic serve --no-open  
ionic serve --devapp  

### fake a  device in browser
ionic cordova run browser  

### Load on device
ionic cordova run android  

### Set JDK to 8 - ubuntu
sudo update-alternatives --config javac

### TODO
* add in club info one results page
* add in the individual pools - mostly done
* add in extra icons from pools - mostly done
* add filters
* add pagination

### Useful sites
* add ability to find pools near me  
    * https://www.freakyjolly.com/ionic-4-get-device-latitude-longitude-and-address-using-geolocation-and-native-geocoder-services-in-ionic-4-native-application/#more-1261
    * https://stackoverflow.com/questions/55652890/module-has-no-exported-member-nativegeocoderreverseresult-in-ionic-3-geo
* https://ionicacademy.com/ionic-4-app-api-calls/  
* https://edupala.com/
* ngrok http 8100 --host-header=rewrite
